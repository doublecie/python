age = int(input("Enter your age: "))

if age <= 12:
    print("It's great to be a kid!")
elif age in range(13, 20):
    print("You're a teenager!")
else:
    print("Time to grow up")

# If any of these statements are true
# the corresponding message will be displayed.
# If neither statement is true, the "else"
# message is displayed.
